using System;
using Dokk.States;
using Raylib;
using static Raylib.Raylib;

namespace Dokk
{
    public class Engine : IDisposable
    {
        private readonly StateManager _stateManager;

        public Engine()
        {
            _stateManager = new StateManager();
        }
        
        public void Init()
        {
            TraceLog((int)TraceLogType.LOG_INFO, "Initializing Dokk engine");
            
            InitWindow(1280, 720, "dokk");
            
            _stateManager.Init(new MainState());
            _stateManager.SetState<MainState>();
        }
        
        public void Run()
        {
            while (!WindowShouldClose())
            {
                Update();
                Input();
                Draw();
            }
        }

        private void Update()
        {
            _stateManager.Update();
        }

        private void Input()
        {
            _stateManager.Input();
        }

        private void Draw()
        {
            BeginDrawing();
            ClearBackground(Color.BLACK);
            
            _stateManager.Draw();
            
            EndDrawing();
        }
        
        public void Dispose()
        {
            CloseWindow();
        }
    }
}