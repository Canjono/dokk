using System.Drawing;
using Raylib;
using Color = Raylib.Color;

namespace Dokk.GameObjects.Ui
{
    public class Button : IButton
    {
        private Text _text;
        private Color _backgroundColor;
        private Color _selectedColor;
        private Color _unselectedColor;
        
        public Point Size { get; set; }
        public Point Position { get; set; }
        public bool MouseIsOver { get; set; }

        public Button()
        {
            Size = new Point(200, 100);
            Position = new Point(Raylib.Raylib.GetScreenWidth() / 2 - Size.X / 2, Raylib.Raylib.GetScreenHeight() / 2 - Size.Y / 2);
            _selectedColor = Color.DARKBLUE;
            _unselectedColor = Color.DARKGRAY;
            _backgroundColor = _unselectedColor;
            _text = new Text(40, Color.GOLD, Position, Size);

            MouseIsOver = false;
        }
        
        public void OnMouseOver()
        {
            Raylib.Raylib.TraceLog((int)TraceLogType.LOG_INFO, "Mouse is over Play button!");
            MouseIsOver = true;
            _backgroundColor = _selectedColor;
        }

        public void OnMouseExit()
        {
            Raylib.Raylib.TraceLog((int)TraceLogType.LOG_INFO, "Mouse exited Play button!");
            MouseIsOver = false;
            _backgroundColor = _unselectedColor;
        }
        
        public void OnClick()
        {
            Raylib.Raylib.TraceLog((int)TraceLogType.LOG_INFO, "Clicked Play Button!");
            _backgroundColor = Color.RED;
        }

        public void Draw()
        {
            Raylib.Raylib.DrawRectangle(Position.X, Position.Y, Size.X, Size.Y, _backgroundColor);
            _text.Draw();
        }
    }
}