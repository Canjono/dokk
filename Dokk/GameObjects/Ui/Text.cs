using System.Drawing;
using Color = Raylib.Color;

namespace Dokk.GameObjects.Ui
{
    public class Text
    {
        private string _text;
        private int _fontSize;
        private Color _color;
        private Point _position;

        public Text(int fontSize, Color color, Point parentPosition, Point parentSize)
        {
            _text = "PLAY";
            _position = new Point(parentPosition.X + parentSize.X / 2 - Raylib.Raylib.MeasureText(_text, fontSize) / 2, parentPosition.Y + parentSize.Y / 2 - fontSize / 2);
            _fontSize = fontSize;
            _color = color;
        }

        public void Draw()
        {
            Raylib.Raylib.DrawText(_text, _position.X, _position.Y, _fontSize, _color);
        }
    }
}