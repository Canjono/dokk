namespace Dokk.GameObjects.Ui
{
    public interface IButton
    {
        void OnMouseOver();
        void OnMouseExit();
        void OnClick();
    }
}