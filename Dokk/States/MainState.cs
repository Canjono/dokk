using Dokk.GameObjects.Ui;
using Raylib;
using static Raylib.Raylib;

namespace Dokk.States
{
    public class MainState : IState
    {
        private Button[] _buttons;
        
        public void Initialize()
        {
            _buttons = new[] {new Button()};
        }

        public void Enter()
        {
        }

        public void Exit()
        {
        }

        public void Update()
        {
        }

        public void Input()
        {
            var mousePosition = GetMousePosition();

            foreach (var button in _buttons)
            {
                var isInsideButton =
                       mousePosition.x > button.Position.X && mousePosition.x < button.Position.X + button.Size.X
                    && mousePosition.y > button.Position.Y && mousePosition.y < button.Position.Y + button.Size.Y;
    
                if (isInsideButton)
                {
                    if (!button.MouseIsOver)
                    {
                        button.OnMouseOver();
                        break;
                    }
                    
                    if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
                    {
                        TraceLog((int)TraceLogType.LOG_INFO, "Click inside button");
                        button.OnClick();
                        break;
                    }
                }
                else if (button.MouseIsOver)
                {
                    button.OnMouseExit();
                }
            }
            
            
        }

        public void Draw()
        {
            foreach (var button in _buttons)
            {
                button.Draw();
            }
        }
    }
}