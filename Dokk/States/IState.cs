namespace Dokk.States
{
    public interface IState
    {
        void Initialize();
        void Enter();
        void Exit();
        void Update();
        void Input();
        void Draw();
    }
}