namespace Dokk.States
{
    public class NullState : IState
    {
        public void Initialize()
        {
        }

        public void Enter()
        {
        }

        public void Exit()
        {
        }

        public void Update()
        {
        }

        public void Input()
        {
        }

        public void Draw()
        {
        }
    }
}