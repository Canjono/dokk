using System;
using System.Collections.Generic;

namespace Dokk.States
{
    public class StateManager
    {
        private readonly Dictionary<Type, IState> _states;
        private IState _stateCurrent;
        private IState _stateScheduled;

        public StateManager()
        {
            _states = new Dictionary<Type, IState>();
        }

        public void Init(params IState[] states)
        {
            foreach (var state in states)
            {
                _states[state.GetType()] = state;
            }

            foreach (var state in states)
            {
                state.Initialize();
            }

            _stateCurrent = new NullState();
        }

        public void Update()
        {
            _stateCurrent.Update();

            if (_stateScheduled == null)
            {
                return;
            }

            _stateCurrent.Exit();
            _stateScheduled.Enter();
            _stateCurrent = _stateScheduled;
            _stateScheduled = null;
        }

        public void Input()
        {
            _stateCurrent.Input();
        }

        public void Draw()
        {
            _stateCurrent.Draw();
        }

        private TState GetState<TState>() where TState : IState
        {
            return (TState)_states[typeof(TState)];
        } 

        public void SetState<TState>() where TState : IState
        {
            _stateScheduled = GetState<TState>();
        }
    }
}