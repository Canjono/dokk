﻿using Dokk.GameObjects.Ui;
using Raylib;
using static Raylib.Raylib;

namespace Dokk
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var engine = new Engine())
            {
                engine.Init();
                engine.Run();
            }

            var playButton = new Button();
            
            while (!WindowShouldClose())
            {
                Input(playButton);
                Update();
                Draw(playButton);
            }

            CloseWindow();
        }

        private static void Input(Button button)
        {
            var mousePosition = GetMousePosition();
            var isInsideButton =
                mousePosition.x > button.Position.X && mousePosition.x < button.Position.X + button.Size.X
                && mousePosition.y > button.Position.Y && mousePosition.y < button.Position.Y + button.Size.Y;

            if (isInsideButton && !button.MouseIsOver)
            {
                button.OnMouseOver();
            }
            else if (!isInsideButton && button.MouseIsOver)
            {
                button.OnMouseExit();
            }
            
            if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
            {
                if (isInsideButton)
                {
                    TraceLog((int)TraceLogType.LOG_INFO, "Click inside button");
                    button.OnClick();
                }
            }
        }

        private static void Update()
        {
            
        }

        private static void Draw(Button button)
        {
            BeginDrawing();
                
            ClearBackground(Color.DARKGRAY);

            button.Draw();
                
            EndDrawing();
        }
    }
}